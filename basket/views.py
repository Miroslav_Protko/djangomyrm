from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.template import loader

from .models import Basket


def home_page(request, **kwargs):
    template = loader.get_template('basket/home_page.html')
    print Basket.objects.all()

    context = {
        "list_of_baskets": Basket.objects.all()
    }

    return HttpResponse(template.render(context, request))


def add_basket(request):
    name_basket = request.POST['basket_name']
    basket_path = request.POST['basket_path']
    basket = Basket(name_basket=name_basket, basket_path=basket_path)
    basket.save()

    parent = request.META["HTTP_REFERER"]
    return HttpResponseRedirect(parent)



