from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home_page, name='home'),
    url(r'^add_basket', views.add_basket, name='add_basket'),
]
